# Peak extractor
Python port of the peak extractor algorithm used for Matrius

## Usage
This software comes with various parameters

`-f`, `--file` path to the ascan. default to `ascan/evident.asc`

`--zero` set the zero value for absolute. default to `2048`

`--average` set the width for the convolution average. default to `5`

`--peaks_ratio` set the threshold ratio multiplier. default to `0.2`

`--peaks_grace` tell the peaks area detection width. default to `10` 

## Algorithm
1. Get absolute signal
2. Get average
3. Define peak threshold
4. Find peak area
5. Get peak area centers

### absolute signal
The signal received from Us-Spi is centered on 2048.
We then shift the signal by 2048 and remove the sign for negative value.

### average
For each value, we sum it with its neighbours then average it

### peak threshold
In order to handle various peak amplitude, we compute the peak detection threshold based on the maximum value measured.

### peak areas
For each value, we check if any of its neighbor exceed the threshold. If yes, it is assigned a peak value

ex: [1, 1, 1, 0, 0, 0, 1, 1, 1] @ width = 1 -> [1, 1, 1, 1, 0, 1, 1, 1, 1]

### peak area centers
For each value from peak area result, we compute the center of each consecutive peaks value.

ex: [1, 1, 1, 0, 1, 1, 1] -> [1, 5]

## Algorithm using Short Time Fourier Transform
1. Compute STFT of the signal values using a hamming window (window size and overlap are empirical so far)
2. Keep the projection onto the time domain
3. Find peaks in the projection using find_peaks function and a defined threshold

Note that the time axis returned by the STFT is different than the one from the acquisition (raw signal). 