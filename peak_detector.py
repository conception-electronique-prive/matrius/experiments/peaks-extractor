import json
from functools import reduce
from scipy import signal
import numpy as np
import matplotlib.pyplot as plt
import argparse


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file', default="ascan/evident.asc", type=str)
    parser.add_argument('--zero', default=2048, type=int)
    parser.add_argument('--average', default=5, type=int)
    parser.add_argument('--peaks_ratio', default=0.2, type=float)
    parser.add_argument('--peaks_grace', default=10, type=int)
    return parser.parse_args()

def extract_samples(file: str):
    with open(file, "r") as f:
        content = f.read()
        return json.loads(content)["measure"]["samples"]
    
def extract_configuration(file: str):
    with open(file, "r") as f:
        content = f.read()
        return json.loads(content)["configuration"]

def get_sampling_frequency(samplingFreq: int):
    """
    Retrieve the sampling frequency in Hz according to Lecoeur US-SPI documentation 
    :param samplingFreq: samplingFreq parameter set during acquisition
    :return: sampling frequency in Hz
    """
    sampling_frequencies = [160e6, 80e6, 40e6, 20e6]
    return  sampling_frequencies[samplingFreq+1]

def get_absolute(samples: list[int], zero: int):
    """
    Shift a signal values to a virtual zero and return their absolute value
    :param samples: signal values
    :param zero: virtual zero
    :return: the abs shifted signal values
    """
    for i in range(0, len(samples)):
        if samples[i] < zero:
            samples[i] = zero - samples[i]
        else:
            samples[i] = samples[i] - zero
    return samples

def get_normalize(samples: list[int], scale:float = 1):
    """
    Normalize a signal values around its mean value 
    :param samples: signal values
    :param scale: scale the normalized signal values
    :return: return the normalized signal values
    """
    samples = samples - np.mean(samples)
    samples = scale*samples/np.max(np.abs(samples))
    return samples

def get_convolution_average(samples: list[int], width: int):
    """
    Perform a convolution average on a signal by averaging each values with its immediate neighbours
    :param samples: signal values to average
    :param width: tell how many neighbours to average per side (left_neighbours + value + right_neighbours)
    :return: average signals values
    """
    average = [0 for _ in range(0, len(samples))]
    for i in range(0, len(samples)):
        start = i - width
        end = i + width
        if start < 0:
            start = 0
        if end > (len(samples)):
            end = len(samples)
        for j in range(start, end):
            average[i] = average[i] + samples[j]
        average[i] = average[i] // (width * 2)
    return average

def get_peak_threshold(samples: list[int], ratio: float):
    """
    return the threshold to use for peak detection
    will find the signal highest value then multiply it by the ratio
    :param samples: signal values
    :param ratio: multiplication factor for signal highest value
    :return: threshold value
    """
    return int(reduce(lambda a, b: max(a, b), samples) * ratio)

def _is_in_peak_area(samples: list[int], index: int, threshold: int, grace: int):
    """
    tell if any of the value and its neighbours exceed the threshold
    :param samples: signal values
    :param index: current value index
    :param threshold: if the value exceed this, will be reported as a peak
    :param grace: number of neighbours to check per side (left_neighbours_width + value + right_neighbours_width)
    :return: true if any of the neighbours exceed the threshold
    """
    start = index - grace
    end = index + grace
    if start < 0:
        start = 0
    if end > (len(samples)):
        end = len(samples)
    return any([x > threshold for x in samples[start:end]])

def get_peaks_by_area_detection(samples: list[int], ratio: float, grace: int):
    """
    Find signal peaks by using area detection.
    First compute a threshold based on signal max value
    Then create a table listing each value exceeding the threshold
    Finally find center value for each groups of consecutive values exceeding the threshold
    :param samples: signal values
    :param ratio: multiplier for the threshold computation (see :py:func:`get_peak_threshold`)
    :param grace: neighbours to check when running the peak detection (see :py:func:`_is_in_peak_area`)
    :return:
    """
    peak_value = 2000
    threshold = get_peak_threshold(samples, ratio)
    peak_areas = [_is_in_peak_area(samples, index, threshold, grace) * peak_value for index in range(0, len(samples))]
    peaks = list()
    start = len(samples)
    for i in range(0, len(samples)):
        point = peak_areas[i]
        if point == peak_value and start > i:
            start = i
        elif point == 0 and start < i:
            peaks.append(((i - start) // 2) + start)
            start = len(samples)
    return peaks

def get_peaks_by_stft(samples: list[int], sampling_frequency: float = 80e6, threshold: float = 0.4):
    """
    Find signal peaks by using Short Time Fourier Transform (STFT).
    First compute STFT of the signal values using a hamming window (nperseg and noverlap are empirical so far)
    Then keep the projection onto the time domain
    Finally find peaks in the projection using find_peaks function and a defined threshold
    :param samples: signal values
    :param sampling_frequency: signal sampling frequency, according to acquisition configuration
    :param threshold: threshold used for the find_peaks function
    :return: stft time axis, stft projection onto the time domain, peaks indices according to stft time axis
    """
    _, time_axis_stft, Zxx = signal.stft(samples, sampling_frequency, window='hamming',nperseg=100,noverlap=95,axis=1)
    stft_time = np.sum(np.abs(Zxx),1)
    stft_peaks, _ = signal.find_peaks(stft_time, height=threshold)
    return time_axis_stft, stft_time, stft_peaks

def detect_peaks(samples: list[int], configuration: dict, zero: int, average: int, peaks_ratio: float, peaks_grace: int):
    """
    Full algorithm for find a signal peaks and printing the result
    :param samples: signal values
    :param configuration: acquisition configuration
    :param zero: zero used for :py:func:`get_absolute`
    :param average: average width used for :py:func:`get_convolution_average`
    :param peaks_ratio: ratio used for :py:func:`get_peaks`
    :param peaks_grace: grace used for :py:func:`get_peaks`
    :return:
    """
    sampling_frequency = get_sampling_frequency(configuration['samplingFreq'])
    sampling_interval = 1/sampling_frequency
    signal_delay = configuration['scaleDelay']*sampling_interval
    signal_stop = (configuration['scaleDelay']+len(samples))*sampling_interval

    time_axis_acquisition = np.arange(signal_delay,signal_stop,sampling_interval)

    absolute = get_absolute(samples, zero)
    average = get_convolution_average(absolute, average)
    peak_threshold = get_peak_threshold(average, peaks_ratio)
    peaks = get_peaks_by_area_detection(average, peaks_ratio, peaks_grace)
    
    normalize = get_normalize(samples, max(absolute))
    time_axis_stft, stft_time, stft_peaks = get_peaks_by_stft(normalize, sampling_frequency, threshold=0.4*max(absolute))

    plt.plot(time_axis_acquisition,absolute, label="absolute")
    plt.plot(time_axis_acquisition,average, label='average')

    plt.plot(time_axis_stft+signal_delay, stft_time, label='stft_time' )
    plt.plot(time_axis_stft[stft_peaks]+signal_delay,stft_time[stft_peaks],'x',label='peaks_stft')

    plt.plot(time_axis_acquisition,[peak_threshold for _ in range(0, len(samples))], label='threshold')
    plt.plot(time_axis_acquisition,[2000 * any(peak == i for peak in peaks) for i in range(0, len(samples))], label='peaks_area_detection')

    plt.legend()
    plt.show()

    print('Peaks using area detection:',time_axis_acquisition[peaks])
    print('Peaks using STFT:',time_axis_stft[stft_peaks]+signal_delay)


if __name__ == '__main__':
    args = parse_args()
    samples = extract_samples(args.file)
    configuration = extract_configuration(args.file)
    detect_peaks(samples, configuration, args.zero, args.average, args.peaks_ratio, args.peaks_grace)
